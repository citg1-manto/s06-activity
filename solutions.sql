-- Run the MySQL/Maria DB in Terminal
mysql -u root -p

-- Create/add a database
CREATE DATABASE bookstore;

-- Select/Use a database;
USE bookstore;

-- Create/Add a tables.
CREATE TABLE AUTHOR(
au_id VARCHAR(50)NOT NULL,
au_lname VARCHAR(50) NOT NULL,
au_fname VARCHAR(50) NOT NULL,
address VARCHAR(100) NOT NULL,
city VARCHAR(50) NOT NULL,
state VARCHAR(10) NOT NULL,
CONSTRAINT chck_id CHECK ( au_id REGEXP '^[0-9()+\\-/*]+$'),
PRIMARY KEY(au_id)
);

-- Insert Data in the database
INSERT INTO AUTHOR (au_id,au_lname,au_fname,address,city,state)
VALUES ('172-32-1179','White','Johnson','10932 Bridge Rd.','Menlo Park','CA'), 
       ('213-46-8915','Green','Marjorie','309 63rd St. #411','Oakland','CA'),
       ('238-95-7766','Carson','Cheryl','589 Darwin Ln.','Berkeley','CA'),
       ('267-41-2394','O''leary','Micheal','22 Cleveland Av. #14','San Jose','CA'),
       ('274-80-9391','Straight','Dean','5420 College Av.','Oakland','CA'),
       ('341-22-1782','Smith','Meander','10 Mississippi Dr.','Lawrence','KS'),
       ('409-56-7008','Bennet','Abraham','6223 Bateman St.','Berkeley','CA'),
       ('427-17-2319','Dull','Ann','3410 Blonde St.','Palo Alto','CA'),
       ('472-27-2349','Gringlesby','Burt','P0 Box 792','Covelo','CA'),
       ('486-29-1786','Locksley','Charlene','18 Broadway Av.','San Francisco','CA');

--Create Database table for PUBLISHER
CREATE TABLE PUBLISHER(
pub_id INT NOT NULL,
pub_name VARCHAR(100),
city VARCHAR(50),
PRIMARY KEY(pub_id)
);

-- Insert Values in the Publisher 
INSERT INTO PUBLISHER (pub_id, pub_name, city)
VALUES (736, 'New Moon Books', 'Boston'),
       (877, 'Binnet & Hardley', 'Washington'),
       (1389, 'Algodata Infosystems', 'Berkeley'),
       (1622, 'Five Lakes Publishing', 'Chicago'),
       (1756, 'Ramona Publisher', 'Dallas'),
       (9901, 'GGG&G', 'Müchen'),
       (9952, 'Scootney Books', 'New York'),
       (9999, 'Lucerne Publishing', 'Paris');


-- Create Database table for TITLE
CREATE TABLE title (
  title_id VARCHAR(6) PRIMARY KEY,
  titles VARCHAR(100) NOT NULL,
  type VARCHAR(20) NOT NULL,
  price DECIMAL(8,2) NOT NULL,
  pub_id INT NOT NULL,
  CONSTRAINT fk_Publisher
    FOREIGN KEY (pub_id) REFERENCES publisher(pub_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
-- INSERT DATA IN THE TABLE TITLE

INSERT INTO TITLE(title_id,titles,type,price,pub_id)
VALUES ('BU1032', 'The Busy Executive''s Database Guide', 'business', 19.99, 1389),
       ('BU1111', 'Cooking with Computers', 'business', 11.95, 1389),
       ('BU2075', 'You Can Combat Computer Stress!', 'business', 2.95, 736),
       ('BU7832', 'Straight Talk About Computers', 'business', 19.99, 1389),
       ('MC2222', 'Silicon Valley Gastronomic Treats', 'mod_cook', 19.99, 877),
       ('MC3021', 'The Gourmet Microwave', 'mod_cook', 2.99, 877),
       ('MC3026', 'The Psychology of Computer Cooking', 'UNDICIDED', '0', 877),
       ('PC1035', 'But Is It User Friendly?', 'popular_comp', 22.95, 1389),
       ('PC8888', 'Secrets of Silicon Valley', 'popular_comp', 20, 1389),
       ('PC9999', 'Net Ettiquette', 'popular_comp','0' , 1389),
       ('PS2091', 'Is Anger the Enemy?', 'psychology', 10.95, 736),
       ('TC7777', 'Mad Titans', 'mod_cook','0', 736);

-- Create Author_Title from the through Foreign Keys
CREATE TABLE AUTHOR_TITLE (
    au_id VARCHAR(50) NOT NULL,
    title_id VARCHAR(6) NOT NULL,
    PRIMARY KEY (au_id, title_id),
    CONSTRAINT fk_author_title_author
    FOREIGN KEY (au_id) REFERENCES AUTHOR (au_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
    CONSTRAINT fk_author_title_title
    FOREIGN KEY (title_id) REFERENCES TITLE (title_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
-- Insert values from the Author_title table
INSERT INTO AUTHOR_TITLE (au_id, title_id)
VALUES ('172-32-1179', 'PS3333'),
       ('213-46-8915', 'BU1032'),
       ('213-46-8915', 'BU2075'),
       ('238-95-7766', 'PC1035'),
       ('267-41-2394', 'BU1111'),
       ('267-41-2394', 'TC7777'),
       ('274-80-9391', 'BU7832'),
       ('409-56-7008', 'BU1032'),
       ('427-17-2319', 'PC8888'),
       ('472-27-2349', 'BU1032');

/*
     a. List books Authored by Marjorie Green

        BU1032  - 'The Busy Executives Database Guide'
        BU2075  - 'You Can Combat Computer Stress'

     b. List the books Authored by Michael O'Leary
       
        BU1111 - 'Cooking with Computers'
     
     c. Write the author/s of "The Busy Executives Database Guide"
      
        213-46-8915 - 'Marjorie Green'
        409-56-7008 - 'Abraham Bennet'

     d. Identify the publisher of "But is it User Friendly"
        
        1389 - 'Algodata Infosystem'

     e. List the books published by Algodata Infosystem
      
        PC1035 - 'The Busy Executives Database Guide'
        BU1032 - 'Cooking with Computers'
        BU1111 - 'Straight Talk About Computers'
        BU7832 - 'But is it User Friendly'
        PC8888 - 'Secrets of Silicon Valley'
        PC9999 - 'Net Etiquette'
*/



-- Create/add a database
CREATE DATABASE blog_db;

-- Select/Use a database;
USE blog_db;

-- Create/add a database

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(50) NOT NULL,
password VARCHAR(50) NOT NULL,
datetime_create DATETIME NOT NULL,
PRIMARY KEY(id)
);

-- Create table for post

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(100) NOT NULL,
content VARCHAR(500) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_author_id
FOREIGN KEY(author_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

-- Create table for post_comments foreign key

CREATE TABLE post_comments(

id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id
FOREIGN KEY(post_id) REFERENCES posts(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
CONSTRAINT fk_user_id
FOREIGN KEY(user_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

-- Create table for post_likes foreign key
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_postlikes_id
FOREIGN KEY(post_id) REFERENCES posts(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
CONSTRAINT fk_userlikes_id
FOREIGN KEY(user_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);
